package eu.europeana.corelib.definitions.model.facets.logic;

/**
 * An enum of possible image orientations.
 */
public enum ImageOrientation {

    LANDSCAPE,
    PORTRAIT

}
